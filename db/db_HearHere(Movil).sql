drop database if exists db_hearhereMovil;
create database db_hearhereMovil;
use db_hearhereMovil;

create table Artista(
	idArtista int not null auto_increment,
	nombre varchar(255),
	primary key (idArtista)
);

create table Album(
	idAlbum int not null auto_increment,
	nombre varchar(255),
	idArtista int,
	primary key(idAlbum),
	foreign key (idArtista) references Artista(idArtista)
);

create table Cancion(
	idCancion int not null auto_increment,
	nombre varchar(255),
	idArtista int,
	idAlbum int,
	ubicacion varchar(255),
	primary key(idCancion),
	foreign key (idArtista) references Artista(idArtista),
	foreign key (idAlbum) references Album(idAlbum)
);

create table ListaDeReproduccion(
	idListaDeReproduccion int not null auto_increment,
	nombre varchar(255),
	primary key (idListaDeReproduccion)
);

create table ListaCancion(
	idLista int not null,
	idCancion int not null,
	primary key (idLista,idCancion),
	foreign key (idLista) references ListaDeReproduccion(idListaDeReproduccion),
	foreign key (idCancion) references Cancion(idCancion)
);

create table Perfil(
	usuario varchar(10) not null,
	pass varchar(255) not null,
	nombre varchar(255) not null,
	foto varchar(255) not null,
	ubicacion varchar(255),
	primary key (idPerfil)
);
