drop database if exists db_hearhereApi;
create database db_hearhereApi;
use db_hearhereApi;

CREATE TABLE perfilApi(
	usuarioApi varchar(10) not null,
	pass varchar(255) not null,
	nombre varchar(255) not null,
	foto varchar(255) not null,
	ubicacion varchar(255),
	primary key(usuarioApi)
);

CREATE TABLE radar(
	idRadar SERIAL ,
	cancion VARCHAR (255),
	usuarioApi VARCHAR(255),
	PRIMARY KEY (idRadar),
	FOREIGN KEY (usuarioApi) REFERENCES PerfilApi(usuarioApi)
);

--insertando Usuaio
INSERT INTO perfilapi(
            usuarioapi, pass, nombre, foto, ubicacion)
    VALUES ('phernandez','123','Pablo','foto1','Guatemala,Guatemala');

INSERT INTO perfilApi (
            usuarioapi, pass, nombre, foto, ubicacion)
    VALUES  ('Diño','123','Ronaldiho','foto2','Guatemala,Guatemala');
    
INSERT INTO perfilApi(
            usuarioapi, pass, nombre, foto, ubicacion)
    VALUES( 'rtn','123','Retana','foto3','Guatemala,Guatemala');


--insertando Usuarios activos
INSERT INTO radar(
			idRadar,cancion,usuarioApi)
	VALUES(0,'Cancion 1','phernandez' );

INSERT INTO radar(
			idRadar,cancion,usuarioApi)
	VALUES(1,'Cancion 2','phernandez' );

INSERT INTO radar(
			idRadar,cancion,usuarioApi)
	VALUES(2,'Cancion 3','phernandez' );

INSERT INTO radar(
			idRadar,cancion,usuarioApi)
	VALUES(3,'Cancion 1','Diño' );

INSERT INTO radar(
			idRadar,cancion,usuarioApi)
	VALUES(4,'Cancion 2','Diño' );

INSERT INTO radar(
			idRadar,cancion,usuarioApi)
	VALUES(5,'Cancion 3','Diño' );

INSERT INTO radar(
			idRadar,cancion,usuarioApi)
	VALUES(6,'Cancion 1','rtn' );

INSERT INTO radar(
			idRadar,cancion,usuarioApi)
	VALUES(7,'Cancion 2','rtn' );

INSERT INTO radar(
			idRadar,cancion,usuarioApi)
	VALUES(8,'Cancion 3','rtn' );