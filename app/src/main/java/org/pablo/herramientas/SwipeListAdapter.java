package org.pablo.herramientas;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.pablo.R;
import org.pablo.model.Usuario;

import java.util.List;

/**
 * Created by PabloHernandez on 11/06/2015.
 */
public class SwipeListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Usuario> userList;
    private String[] bgColors;

    public SwipeListAdapter(Activity activity, List<Usuario> movieList) {
        this.activity = activity;
        this.userList= movieList;
        bgColors = activity.getApplicationContext().getResources().getStringArray(R.array.user_serial_bg);
    }


    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int i) {
        return userList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null)
            view = inflater.inflate(R.layout.contenido_layout, null);


        TextView title = (TextView) view.findViewById(R.id.idTitulo);

        title.setText(userList.get(i).getNombre());

        return view;
    }
}
