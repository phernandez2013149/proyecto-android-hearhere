package org.pablo.activity;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.pablo.R;
import org.pablo.http.ConsultaUsuario;
import org.pablo.http.NuevoUsuario;
import org.pablo.model.Usuario;

import java.util.concurrent.ExecutionException;

public class Registro extends Activity {

    EditText editUsuario,editNombre,editPass,editPass2;
    Button btnRegistrar;

    public Registro(){}

    Usuario usuario;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        editUsuario=(EditText) findViewById(R.id.editUsuario);
        editNombre=(EditText)findViewById(R.id.editNombre);
        editPass=(EditText)findViewById(R.id.editPass);
        editPass2=(EditText)findViewById(R.id.editPass2);
        btnRegistrar=(Button)findViewById(R.id.btnRegistrar);
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    onRegistrar();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_registro, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onRegistrar() throws ExecutionException, InterruptedException {
    //   Boolean a= new ConsultaUsuario().execute(editUsuario.getText().toString()).get();
//        Boolean b= new ConsultaUsuario().e    xecute(editUsuario.getText().toString()).get();

        if(editPass.getText().toString().equals(editPass2.getText().toString()) ){
            NuevoUsuario usr=new NuevoUsuario();
            try {
                usr.execute(editUsuario.getText().toString(), editPass.getText().toString(), editNombre.getText().toString(), "foto", "Guatemala,Guatemala").get();
                usr.cancel(true);
                startActivity(new Intent(Registro.this, Login.class));
            }catch (Exception e){
                Log.e("ERROR-LOGINEXEC", "" + e);
            }

        }else {
            Toast.makeText(getApplicationContext(),"Las contraseņas no coinciden",Toast.LENGTH_SHORT).show();
        }

    }

}
