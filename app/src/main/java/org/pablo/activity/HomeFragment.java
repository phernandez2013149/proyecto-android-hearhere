package org.pablo.activity;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import org.pablo.R;
import org.pablo.http.LoginConexion;
import org.pablo.model.Usuario;

import java.util.concurrent.ExecutionException;

/**
 * Created by pabloHernandez on 30/05/2015.
 */
public class HomeFragment extends Fragment {

    private TextView textContent;
    private Usuario logged;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        textContent = (TextView) rootView.findViewById(R.id.textContent);
        cargarDatos();
        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void cargarDatos( ) {
        LoginConexion autenticar=new LoginConexion();
        try {
            logged=autenticar.execute("phernandez","123").get();
        }catch (InterruptedException | ExecutionException e){
            Log.e("ERROR-LOGINEXEC", "" + e);
        }

        if(logged!=null){
            textContent.setText(logged.getNombre());

        }else{
           textContent.setText("No hay conexion");
        }

}
}
