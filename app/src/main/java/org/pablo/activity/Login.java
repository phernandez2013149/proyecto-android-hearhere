package org.pablo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.pablo.R;
import org.pablo.http.LoginConexion;
import org.pablo.model.Loged;
import org.pablo.model.Usuario;


import java.util.concurrent.ExecutionException;

public class Login extends Activity {
    private Usuario logged;
    private TextView textNombreUsuario;
    private EditText editUsuario,editPass;
    private Button btnLogin;
    private TextView textRegistrame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        editUsuario=(EditText)findViewById(R.id.editUsuario);
        editPass=(EditText)findViewById(R.id.editPass);
        textNombreUsuario=(TextView) findViewById(R.id.textNombreUsuario);
        btnLogin=(Button) findViewById(R.id.btnLogin);
        textRegistrame=(TextView) findViewById(R.id.textRegistarme);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickLogin();
            }
        });
        textRegistrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Login.this,Registro.class));
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void onClickLogin() {
        LoginConexion autenticar=new LoginConexion();
        try {
            logged=null;
            logged=autenticar.execute(editUsuario.getText().toString(),editPass.getText().toString()).get();
        }catch (InterruptedException | ExecutionException e){
            Log.e("ERROR-LOGINEXEC", "" + e);
        }

        if(logged!=null){
            Toast.makeText(getApplicationContext(),"Bienvenido "+logged.getNombre()+"(puntoExtra)",Toast.LENGTH_SHORT).show();
            textNombreUsuario.setText(logged.getNombre());
            textNombreUsuario.setVisibility(View.VISIBLE);
            btnLogin.setVisibility(View.INVISIBLE);
            editPass.setVisibility(View.INVISIBLE);
            editUsuario.setVisibility(View.INVISIBLE);
            textRegistrame.setVisibility(View.INVISIBLE);

        }else{
            Toast.makeText(getApplicationContext(),"Verifique sus credenciales", Toast.LENGTH_SHORT).show();
        }

        }

}
