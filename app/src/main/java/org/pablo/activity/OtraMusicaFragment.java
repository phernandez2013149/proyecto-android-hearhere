package org.pablo.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.pablo.R;
import org.pablo.herramientas.SwipeListAdapter;
import org.pablo.model.Usuario;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pabloHernandez on 30/05/2015.
 */

public class OtraMusicaFragment extends Fragment {
    private View rootView;
    private ListView lista;
    private List<Usuario> listaDeUsuario;
    private SwipeListAdapter adapter;


    public OtraMusicaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView=inflater.inflate(R.layout.otra_musica_friends, container, false);
        lista=(ListView) rootView.findViewById(R.id.listView);
      //  lista.addHeaderView(R.layout.contenido_layout);
        listaDeUsuario= new ArrayList<>();

        return rootView;
    }
}
