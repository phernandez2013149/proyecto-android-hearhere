package org.pablo.model;

/**
 * Created by PabloHernandez on 03/06/2015.
 */
public class Usuario {
    private String usuarioApi;
    private String pass;
    private String nombre;
    private String foto;
    private String ubicacion;

    public Usuario(String usuarioApi, String pass, String nombre, String foto,String ubicacion) {
        this.usuarioApi = usuarioApi;
        this.pass = pass;
        this.nombre = nombre;
        this.foto=foto;
        this.ubicacion = ubicacion;
    }

    public Usuario(){}

    public String getUsuarioApi() {
        return usuarioApi;
    }

    public void setUsuarioApi(String usuarioApi) {
        this.usuarioApi = usuarioApi;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto= foto;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
}
