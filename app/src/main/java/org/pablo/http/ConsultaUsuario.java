package org.pablo.http;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by PabloHernandez on 06/06/2015.
 */



public class ConsultaUsuario extends AsyncTask<String,Integer,Boolean>{
    private String URL_LOGIN_API="http://192.168.56.1:3000/auth/";

    @Override
    protected Boolean doInBackground(String... strings) {
        HttpClient httpClient= new DefaultHttpClient();
        HttpPost httpPost= new HttpPost(URL_LOGIN_API+"aprobarUsuario");
        httpPost.setHeader("Content-type","application/json");
        httpPost.setHeader("charset","utf-8");
        JSONObject data=new JSONObject();

        try {
            data.put("usuarioApi",strings[0]);
            StringEntity stringEntity=new StringEntity(data.toString());
            httpPost.setEntity(stringEntity);
            HttpResponse respuesta=httpClient.execute(httpPost);
            JSONArray listaDeDatos=new JSONArray(EntityUtils.toString(respuesta.getEntity()));
            data=null;
            for (int i=0;i<listaDeDatos.length();i++){
                if(listaDeDatos.length()==0)
                    return true;

            }
        }catch (JSONException e){
            Log.e("doInBa-JSONEXCEPTION : ", "" + e);
        }catch (UnsupportedEncodingException e){
            Log.e("doInBa-ENCODING: ",""+e);
        }catch(ClientProtocolException e){
            Log.e("doInBa-CLIENT: ",""+e);
        }catch (IOException e){
            Log.e("doInBa-IO: ",""+e);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {

        super.onPostExecute(aBoolean);

    }
}
