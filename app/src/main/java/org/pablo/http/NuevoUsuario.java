package org.pablo.http;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.pablo.model.Usuario;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by PablHernandez on 07/06/2015.
 */
public class NuevoUsuario  extends AsyncTask<String, Integer,Void>{
    private static String URL_LOGIN_API="http://192.168.56.1:3000/auth/nuevo";
    @Override
    protected Void doInBackground(String... usuarios) {

        HttpClient httpClient = new DefaultHttpClient();
        HttpPost post = new HttpPost(URL_LOGIN_API);
        post.setHeader("Content-type","application/json");
        post.setHeader("charset","utf-8");
        JSONObject data=new JSONObject();
        try {
            data.put("usuarioApi", usuarios[0]);
            data.put("pass",usuarios[1]);
            data.put("nombre",usuarios[2]);
            data.put("foto", usuarios[3]);
            data.put("ubicacion", usuarios[4]);
            post.setEntity(new StringEntity(data.toString()));
            httpClient.execute(post);

        }catch (JSONException e){
            Log.e("doInBa-JSONEXCEPTION : ", "" + e);
        }catch (UnsupportedEncodingException e){
            Log.e("doInBa-ENCODING: ",""+e);
        }catch(ClientProtocolException e){
            Log.e("doInBa-CLIENT: ",""+e);
        }catch (IOException e){
            Log.e("doInBa-IO: ", "" + e);
        }
return  null;
    }
}
