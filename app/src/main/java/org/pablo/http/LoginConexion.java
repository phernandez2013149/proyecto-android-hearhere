package org.pablo.http;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.pablo.model.Loged;
import org.pablo.model.Usuario;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by PabloHernandez on 03/06/2015.
 */
public class LoginConexion extends AsyncTask<String,Integer,Usuario>  {
    private static String URL_LOGIN_API="http://192.168.56.1:3000/auth/login";
    @Override
    protected Usuario doInBackground(String... params) {

        HttpClient httpClient = new DefaultHttpClient();
        HttpPost post = new HttpPost(URL_LOGIN_API);
        post.setHeader("Content-type","application/json");
        post.setHeader("charset","utf-8");
        JSONObject data=new JSONObject();
        try {
            data.put("usuarioApi", params[0]);
            data.put("pass", params[1]);
            post.setEntity(new StringEntity(data.toString()));
            JSONArray listaDeDatos=new JSONArray(EntityUtils.toString(httpClient.execute(post).getEntity()));

            data=null;
            for (int i=0;i<listaDeDatos.length();i++){
                data=listaDeDatos.getJSONObject(i);
                return new Usuario(
                        data.getString("usuarioapi"),
                        data.getString("pass"),
                        data.getString("nombre"),
                        data.getString("foto"),
                        data.getString("ubicacion")
                );
            }
        }catch (JSONException e){
            Log.e("doInBa-JSONEXCEPTION : ",""+e);
        }catch (UnsupportedEncodingException e){
            Log.e("doInBa-ENCODING: ",""+e);
        }catch(ClientProtocolException e){
            Log.e("doInBa-CLIENT: ",""+e);
        }catch (IOException e){
            Log.e("doInBa-IO: ", "" + e);
        }
        return null;
    }
}
